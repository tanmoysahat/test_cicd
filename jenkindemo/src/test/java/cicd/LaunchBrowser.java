package cicd;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.LoadLibs;

public class LaunchBrowser {

	@Test
	public void login() throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/nget/train-search");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//a[@id='loginText']")).click();
		//Thread.sleep(8000);
		//driver.findElement(By.xpath("//div[@class='nlpRefresh']")).click();
		Thread.sleep(8000);
		String filePath = System.getProperty("user.dir")+"\\image\\";
		long imageName = System.currentTimeMillis();
		File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);           
		try {
			// now copy the  screenshot to desired location using copyFile method
			System.out.println("name is "+imageName);
			FileUtils.copyFile(src, new File(filePath+imageName+".png"));
		} catch (IOException e){
			System.out.println(e.getMessage());
		}
		String result = null;
		File imageFile = new File(filePath, imageName+".png");
		System.out.println("Image name is :" + imageFile.toString());
		ITesseract instance = new Tesseract();
		File tessDataFolder = LoadLibs.extractTessResources("tessdata"); // Extracts
		instance.setDatapath(tessDataFolder.getAbsolutePath()); 
		try {
			result = instance.doOCR(imageFile);
		} catch (TesseractException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("image text is "+result);
		driver.quit();
	}
}
